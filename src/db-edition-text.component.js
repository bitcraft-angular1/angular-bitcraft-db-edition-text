/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition-text')
    .component('bitcraftDbEditionText', {
        templateUrl: './js/db-edition-checkboxes-text/db-edition-checkboxes-text.template.html', // this line will be replaced
        bindings: {
            value: '=',
            edit: '<'
        }
    });
